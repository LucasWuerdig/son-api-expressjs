(function($) {
    console.log('hello', $)
    $(document).ready(function() {
        const populateCategory = function () {
            $.get('http://localhost:3000/categories', function (response) {
                if (! response.data.length && ! response.status) {
                    console.log('No category results')
                    return 
                }
                response.data.forEach(function (category) {
                    let tmpl = `<option value="${category._id}">${category.name}</option>`
                    $('#select_category').append(tmpl)
                })
            })
        }
        
        const createData = function () {
            let title = $('input[name="title"]').val()
            let price = $('input[name="price"]').val()
            let category = $('#select_category').val()
            let cep = $('input[name="cep"]').val()

            if (! title || ! price || ! category) {
                console.log('invalid body')
                return false;
            }

            $.post('http://localhost:3000/bills', {
                title: title,
                price: price,
                category: category,
                cep: cep
            }, function(response) {
                console.log(response)
                $('#list-table tbody').empty()
                listData()
            })
        }
        
        const listData = function() {
            $.get('http://localhost:3000/bills', function () {
            })
            .then(function(response) {
                
                if (! response.data.length) {
                    return
                }

                response.data.forEach(function (bill) {
                    category = ''
                    if (bill.category) {
                        category = bill.category.name
                    }
                    
                    let tmpl = '<tr>'+
                               '   <td>' + bill.title + '</td>' +
                               '   <td>' + bill.price + '</td>' +
                               '   <td><a target="_blank" href="http://localhost:3000/address/' + bill.cep + '">' + bill.cep + '</a></td>' +
                               '   <td>' + category + '</td>' +
                               '   <td> <button id="btn-delete" type="button" class="btn btn-danger btn-small" data-id="' + bill._id + '">Delete</button> </td>' +
                               '</tr>'
                    $('#list-table tbody').append(tmpl)
                });

                
            })
        }

        const removeData = function() {
            let id = $(this).data('id')
            $.ajax({
                url: 'http://localhost:3000/bills/' + id,
                type: 'DELETE',
                success: function() {
                    $('#list-table tbody').empty()
                    listData()
                }
            })
        }

        const createDataCategory = function () {
            let name = $('input[name="name"]').val()

            if (! name) {
                console.log('invalid body')
                return
            }

            $.post('http://localhost:3000/categories', { name: name }, function (response) {
                console.log(response)
                $('input[name="name"]').val('')
            })
        }

        listData()
        populateCategory()
        $('#btn-create').on('click', createData)
        $('#btn-create-category').on('click', createDataCategory)
        $('#list-table tbody').on('click', '#btn-delete', removeData)
    });
})(jQuery)